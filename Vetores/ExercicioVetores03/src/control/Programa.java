package control;

import java.util.Scanner;

public class Programa {
	public static void main(String[] args) {

		int vetor[];
		int num;
		num = 0;
		vetor = new int[10];
		Scanner leitura = new Scanner(System.in);

		for (int i = 0; i < 10; i++) {
			System.out.println("Digite o valor para o vetor[" + (i + 1) + "]:");
			num = leitura.nextInt();
			if (num % 2 == 0) {
				vetor[i] = 0;
			} else
				vetor[i] = 1;
		}
		for (int i = 0; i < 10; i++) {
			System.out.println("Vetor [" + (i + 1) + "]: " + vetor[i]);
		}
	}
}
